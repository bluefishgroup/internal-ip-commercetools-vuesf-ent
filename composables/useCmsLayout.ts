import { useContent } from '@vsf-enterprise/contentful'
import { computed } from '@nuxtjs/composition-api'

const useCmsLayout = () => {
  const { search: searchStyleGuide, content: styleGuide } =
    useContent('style-guide')
  const { search: searchLayout, content: layout } = useContent('layout')

  const getLayout = () =>
    Promise.all([
      searchStyleGuide({
        custom: {
          type: 'styleGuide',
          field: 'title',
          value: 'Page Style Guide',
        },
      }),
      searchLayout({
        custom: {
          type: 'layout',
          field: 'title',
          value: 'cms-layout',
        },
      }),
    ])

  const getLayoutPart = (part) =>
    computed(() =>
      layout.value.length && layout.value[0].fields[part]
        ? [layout.value[0].fields[part]]
        : [],
    )

  return {
    getLayout,
    styleGuide,
    header: getLayoutPart('header'),
    footer: getLayoutPart('footer'),
  }
}

export default useCmsLayout

