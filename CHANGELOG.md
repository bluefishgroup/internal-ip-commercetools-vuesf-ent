# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.4.4 (2022-01-04)

**Note:** Version bump only for package @vsf-enterprise/commercetools-theme





## 1.4.3-alpha.2 (2022-01-04)

**Note:** Version bump only for package @vsf-enterprise/commercetools-theme





## 1.4.3-alpha.1 (2022-01-04)

**Note:** Version bump only for package @vsf-enterprise/commercetools-theme





## 1.4.3-alpha.0 (2022-01-04)

**Note:** Version bump only for package @vsf-enterprise/commercetools-theme
