const CTP_PROJECT_KEY='bfg-commercetools-test'
const CTP_CLIENT_SECRET='7jnObTKJdsQlXJvlR3vk-6Zv5jiAvz-y'
const CTP_CLIENT_ID='Bz43_NXGMGo4s5Q7AE4XTUmM'
const CTP_AUTH_URL='https://auth.us-central1.gcp.commercetools.com'
const CTP_API_URL='https://api.us-central1.gcp.commercetools.com'
const CTP_SCOPES='manage_project:bfg-commercetools-test'


module.exports = {
  integrations: {
    ct: {
      location: '@vsf-enterprise/commercetools-api/server',
      configuration: {
        api: {
          uri: `${CTP_API_URL}/${CTP_PROJECT_KEY}/graphql`,
          authHost: CTP_AUTH_URL,
          projectKey: CTP_PROJECT_KEY,
          clientId: CTP_CLIENT_ID,
          clientSecret: CTP_CLIENT_SECRET,
          scopes: CTP_SCOPES.split(' ')
        },
        currency: 'USD',
        country: 'US'
      },
      customQueries: {
        "LastModifiedProductsQuery": ({ query, variables, metadata }) => {
          variables.where = `lastModifiedAt <= "${metadata.date}"`;
          variables.limit = metadata.limit;
          variables.sort = `lastModifiedAt ${metadata.order}`;
          return { query, variables };
        }
      }
    },
    'cntf': {
      location: '@vsf-enterprise/contentful/server',
      configuration: {
        token: 'fIOEpSz76I9y1uNFtBbe8cBreVkbwwF-MMM-khVkjnc',
        space: 'rokqzl5p3790',
      },
    },
  }
};
